/*=========== JS Starts =========*/
$(document).ready(function(){
	$('.navbar-nav a').on('click',function(){
		$('.navbar-nav a').removeClass('active-class');
		$(this).addClass('active-class');
	});
	//Dropdown Hover Animation
	$('.dropdown').click(function(){
		$('.dropdown-menu').addClass('slideInDown');
	});
	// Tooltip 
	$('[data-toggle="tooltip"]').tooltip();
});
// Side-Nav Tab
function openNav() {
    document.getElementById("mySidenav").style.width = "100px";
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
// Mobile Navbar Tab
function openMob() {
    document.getElementById("myMobilenav").style.width = "100%";
}
function closeMob() {
    document.getElementById("myMobilenav").style.width = "0";
}
/*=========== JS Ends =========*/